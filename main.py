from external_libraries.mutation_finder.mutation_finder import mutation_finder_from_regex_filepath, default_regular_expression_filepath, extract_mutations_from_string
from enum import Enum
from flashtext import KeywordProcessor
import pandas as pd
import sys


class EntityType(Enum):
    PROTEIN = 1
    SPECIES = 2


def get_output_file_path_by_entity_type(entity_type):
    entities_list = [EntityType.PROTEIN, EntityType.SPECIES]
    assert(entity_type in entities_list)

    if entity_type == EntityType.PROTEIN:
        return 'results/proteins.out'
    elif entity_type == EntityType.SPECIES:
        return 'results/species.out'


class PDBDatabase:
    PDB_DB_PATH = 'data/databases/pdbfull.processed.txt'
    DB_ENTITIES = [EntityType.PROTEIN, EntityType.SPECIES]

    def __init__(self, file_path=PDB_DB_PATH):
        self._pdb_data_frame = pd.read_csv(
                    file_path,
                    delimiter=' ### '
                ).drop(
                    [
                        'RESOLUTION', 'DEPOSITION DATE', 'MOL_ID as in COMPND and SOURCE',
                        'MOL_ID CHAIN', 'MOL_ID EC', 'MOL_ID IF MUT', 'LIGANDS'
                    ],
                    1
                )


    def create_entity_dict(self, entity_type):
        assert(entity_type in self.DB_ENTITIES)

        entity_dict = {}
        if entity_type == EntityType.PROTEIN:
            for _, row in self._pdb_data_frame.iterrows():
                entity_dict[str(row['PDB'])] = [
                        str(row['PDB']),
                        str(row['MOL_ID NAME'])
                ]
        elif entity_type == EntityType.SPECIES:
            for _, row in self._pdb_data_frame.iterrows():
                entity_dict[str(row['PDB'])] = [
                        str(row['PDB']),
                        str(row['MOL_ID ORG_SCI']),
                        str(row['MOL_ID ORG_COM'])
                ]

        return entity_dict


class EntitiesExtractor:
    def __init__(self):
        self._pdb_database = PDBDatabase()
        self._entities_dicts = {}
        self._entities_extractors = {}

        for entity_type in self._pdb_database.DB_ENTITIES:
            self._entities_dicts[entity_type] = self._pdb_database \
                    .create_entity_dict(entity_type)
            self._entities_extractors[entity_type] = KeywordProcessor()
            self._entities_extractors[entity_type].add_keywords_from_dict(
                    self._entities_dicts[entity_type]
            )


    def extract_entity(self, entity_type, text):
        extracted_entities = self._entities_extractors[entity_type] \
                   .extract_keywords(text, span_info=True)
        result_entities = []
        for entity, begin_pos, end_pos in extracted_entities:
            if end_pos - begin_pos > 2:
                result_entities.append(tuple([entity, begin_pos, end_pos]))

        return result_entities


    def print_entities(self, entity_type, entities, text, output_file=sys.stdout):
        print("EntityType: {}".format(entity_type), file=output_file)
        for key, text_pos_begin, text_pos_end in entities:
            print(
                    'Key: "{}", Value: {}, Match: "{}"'.format(
                        key,
                        self._entities_dicts[entity_type][key],
                        text[text_pos_begin:text_pos_end]
                    ),
                    file=output_file
            )


if __name__ == '__main__':
    input_texts_file_path = "data/corpora/mutation_finder/devo_set.txt"
    input_texts_file = open(input_texts_file_path, 'r')
    output_file_path = "results/output.out"
    output_file = open(output_file_path, 'w')
    mutation_extractor = mutation_finder_from_regex_filepath("external_libraries/mutation_finder/" + default_regular_expression_filepath)

    print("files created")

    extractor = EntitiesExtractor()

    entities_to_extract = [EntityType.PROTEIN, EntityType.SPECIES]

    print("entities_extractor created")


    for num, line in enumerate(input_texts_file):
        print("{} started".format(num))
        fields = line.strip().split('\t')
        identifier = fields[0]
        print("TextId: {}".format(identifier), file=output_file)

        text = '\t'.join(fields[1:])
        result = extract_mutations_from_string(text, mutation_extractor)
        mutations = '\t'.join(map(str, dict(result).keys()))
        print("Mutations: {}".format(mutations), file=output_file)

        for entity_type in entities_to_extract:
            #entity_output_file_path = get_output_file_path_by_entity_type(entity_type)
            #with open(entity_output_file_path, 'a') as entity_output_file:
            extracted_entities = extractor.extract_entity(entity_type, text)
            extractor.print_entities(
                    entity_type,
                    extracted_entities,
                    text,
                    output_file=output_file
            )

        print('\n', file=output_file)
        print("{} finished".format(num))

    input_texts_file.close()
    output_file.close()

